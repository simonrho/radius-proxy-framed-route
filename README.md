### Radius proxy

[![N|Solid](https://www.juniper.net/assets/img/logos/juniper/juniper-networks-white-s.png)](https://www.juniper.net/us/en/)

#### What?
This script appends the framed-route radius attribute(22) when the framed-ip-address value matches the next-hop address 
of the static routes configured in the JUNOS BNG systems to implement the BNG locally sourced framed-routes support.
Note that the junos radius proxy script posted here inherits all features of the script posted on the gitlab link: https://gitlab.com/simonrho/radius-proxy-username


#### Framed-Route attribute (8) source priority
If the access-response message from a radius server includes the framed-ip-address attribute, the radius proxy script will search for 
local static routes pointing the framed-ip-address as the next-hop and insert new framed-route(s) into the access-response message.

However, if the radius server returns the framed-route attribute(s) for the access-response message, 
the script will skip the locally sourced framed-route processing.

#### default junos routing-instance having the static-routes configuration as the local framed-route source
The rouging-instance name 'jrp' is hard-corded in the script to create/read/update/delete the static routes as the local framed-route source.
The metric value of static route will be transalted as the metric value of framed-route.

Example:
```shell
routing-instances {
    jrp {
        routing-options {
            static {
                route 90.0.0.0/24 {     
                    next-hop 203.0.0.100;
                    metric 100;
                }
                route 90.0.1.0/24 {
                    next-hop 203.0.0.100;
                    metric 200;
                }
                route 90.0.2.0/24 next-hop 203.0.0.100;
                route 90.0.3.0/24 next-hop 203.0.0.100;
                route 90.0.4.0/24 {
                    next-hop 203.0.0.100;
                    metric 200;
                }
                route 91.0.0.0/24 {
                    next-hop 203.0.0.101;
                    metric 100;
                }
                route 91.0.1.0/24 {
                    next-hop 203.0.0.101;
                    metric 200;
                }
                route 91.0.2.0/24 next-hop 203.0.0.101;
                route 91.0.3.0/24 next-hop 203.0.0.101;
            }
        }
    }
}
```

#### Radius server: users file example
```shell
root@ix:/etc/freeradius/3.0# cat users 
juniper             Auth-Type := Accept
                    Framed-Ip-Address := "203.0.0.100",
                    Reply-Message = "Ok"


08:00:27:72:0c:be   Auth-Type := Accept
                    Framed-Ip-Address := "203.0.0.101",
                    Reply-Message = "Ok"
```

#### Routing table output

```shell
regress@alpha> show route protocol access-internal 

inet.0: 16 destinations, 16 routes (16 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

203.0.0.100/32     *[Access-internal/12] 00:05:07
                       Private unicast
203.0.0.101/32     *[Access-internal/12] 00:04:47
                       Private unicast

mgmt_junos.inet.0: 3 destinations, 3 routes (3 active, 0 holddown, 0 hidden)

inet6.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)

regress@alpha> show route protocol access             

inet.0: 16 destinations, 16 routes (16 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

90.0.0.0/24        *[Access/13] 00:05:10, metric 100
                       Private unicast
90.0.1.0/24        *[Access/13] 00:05:10, metric 200
                       Private unicast
90.0.2.0/24        *[Access/13] 00:05:10, metric 1
                       Private unicast
90.0.3.0/24        *[Access/13] 00:05:10, metric 1
                       Private unicast
90.0.4.0/24        *[Access/13] 00:05:10, metric 200
                       Private unicast
91.0.0.0/24        *[Access/13] 00:04:50, metric 100
                       Private unicast
91.0.1.0/24        *[Access/13] 00:04:50, metric 200
                       Private unicast
91.0.2.0/24        *[Access/13] 00:04:50, metric 1
                       Private unicast
91.0.3.0/24        *[Access/13] 00:04:50, metric 1
                       Private unicast

mgmt_junos.inet.0: 3 destinations, 3 routes (3 active, 0 holddown, 0 hidden)

inet6.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)


```

#### show subscriber output

```shell
regress@alpha> show subscribers 
Interface             IP Address/VLAN ID                      User Name                      LS:RI
demux0.3221225676     0x8100.100 0x8100.200                   xe-0/1/0:100-200          default:default      
pp0.3221225677        203.0.0.100                             juniper                   default:default      
demux0.3221225678     203.0.0.101                             0800.2772.0cbe            default:default      

regress@alpha> show subscribers extensive 
Type: VLAN
User Name: xe-0/1/0:100-200
Logical System: default
Routing Instance: default
Interface: demux0.3221225676
Interface type: Dynamic
Underlying Interface: xe-0/1/0
Dynamic Profile Name: vlan-demux
Dynamic Profile Version: 2
State: Active
Radius Accounting ID: 227
Session ID: 227
PFE Flow ID: 282
Stacked VLAN Id: 0x8100.100
VLAN Id: 0x8100.200
Login Time: 2021-05-20 05:09:07 UTC

Type: PPPoE
User Name: juniper
IP Address: 203.0.0.100
IP Netmask: 255.255.255.255
Logical System: default
Routing Instance: default
Interface: pp0.3221225677
Interface type: Dynamic
Underlying Interface: demux0.3221225676
Dynamic Profile Name: pppoe-profile1
Dynamic Profile Version: 1
MAC Address: 08:00:27:72:0c:be
State: Active
Radius Accounting ID: 228
Session ID: 228
PFE Flow ID: 284
Stacked VLAN Id: 100
VLAN Id: 200
Login Time: 2021-05-20 05:09:07 UTC
IP Address Pool: p1
Accounting interval: 0                  
Dynamic configuration: 
  junos-framed-route-ip-address-prefix: 90.0.0.0/24
      junos-framed-route-nexthop: 203.0.0.100
          junos-framed-route-cost: 100
  junos-framed-route-ip-address-prefix: 90.0.1.0/24
      junos-framed-route-nexthop: 203.0.0.100
          junos-framed-route-cost: 200
  junos-framed-route-ip-address-prefix: 90.0.2.0/24
      junos-framed-route-nexthop: 203.0.0.100
          junos-framed-route-cost: 1
  junos-framed-route-ip-address-prefix: 90.0.3.0/24
      junos-framed-route-nexthop: 203.0.0.100
          junos-framed-route-cost: 1
  junos-framed-route-ip-address-prefix: 90.0.4.0/24
      junos-framed-route-nexthop: 203.0.0.100
          junos-framed-route-cost: 200

Type: DHCP
User Name: 0800.2772.0cbe
IP Address: 203.0.0.101
IP Netmask: 255.255.255.0
Domain name server inet: 8.8.8.8 
Logical System: default
Routing Instance: default
Interface: demux0.3221225678
Interface type: Dynamic
Underlying Interface: demux0.3221225676
Dynamic Profile Name: dhcp-profile1
Dynamic Profile Version: 1
MAC Address: 08:00:27:72:0c:be
State: Active
Radius Accounting ID: 229
Session ID: 229
PFE Flow ID: 285
Stacked VLAN Id: 100
VLAN Id: 200
Login Time: 2021-05-20 05:09:27 UTC     
DHCP Options: len 33
35 01 01 32 04 cb 00 00 65 0c 07 63 6c 69 65 6e 74 31 37 0d
01 1c 02 03 0f 06 77 0c 2c 2f 1a 79 2a
DHCP Header: len 44
01 01 06 00 a6 19 f2 7a 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 08 00 27 72 0c be 00 00 00 00 00 00
00 00 00 00
IP Address Pool: p1
Accounting interval: 0
Dynamic configuration: 
  junos-framed-route-ip-address-prefix: 91.0.0.0/24
      junos-framed-route-nexthop: 203.0.0.101
          junos-framed-route-cost: 100
  junos-framed-route-ip-address-prefix: 91.0.1.0/24
      junos-framed-route-nexthop: 203.0.0.101
          junos-framed-route-cost: 200
  junos-framed-route-ip-address-prefix: 91.0.2.0/24
      junos-framed-route-nexthop: 203.0.0.101
          junos-framed-route-cost: 1
  junos-framed-route-ip-address-prefix: 91.0.3.0/24
      junos-framed-route-nexthop: 203.0.0.101
          junos-framed-route-cost: 1

```

#### Input arguments format
The radius proxy application supports two options.
1. -s : source address of radius packet sent to radius server. if this value is not defined, the script will derive packet src address from proxy mapping table.
2. -t : timeout value for customer's radius server response. Default value is 3 seconds.
3. -m : radius proxy mapping entries. multiple entries can be specified with comma separator (',')
>   \<local ip>:\<local port>-<remote ip>:\<remote port>-\<radius secret>, ...
   
  The number of mapping entry is unlimited.
  You can copy jrp.py and rename it and use it if you want to have application redundancy.
  For instance, 
  > jrp1.py for 1.1.1.1:11812-10.0.0.1:1812-juniper<br/>
  > jrp2.py for 1.1.1.1:11813-10.0.0.1:1813-juniper<br/>
  > jrp3.py for 1.1.1.1:21812-10.0.0.1:1812-juniper<br/>
  > jrp4.py for 1.1.1.1:21813-10.0.0.1:1813-juniper
  
  The radius secret key can be a <strong>plain string</strong> or <strong>JUNOS encrypted format</strong> value (starting with $9$ prefix)
  You can copy JUNOS encrypted secret key from radius-server configuration on Junos configuration mode CLI
 
#### installation steps
1. copy jrp.py into MX BNG system.
2. register jrp.py file into JET application with radius proxy mapping arguments.
3. add/update JUNOS radius client configuration pointing new radius proxy server listen ip and port. 

```shell
$ scp jrp.py user@bng1:
$ ssh user@bng1
Last login: Mon Mar 22 03:41:04 2021 from 10.107.36.101
--- JUNOS 20.2R1.10 Kernel 64-bit  JNPR-11.0-20200608.0016468_buil
user@bng1> 
user@bng1> request system scripts refresh-from extension-service file jrp.py url /var/home/user/jrp.py 
refreshing 'jrp.py' from '/var/home/user/jrp.py'
user@bng1> 
user@bng1> file list /var/db/scripts/jet/jrp.py detail 
-rw-r-----  1 root  wheel       9283 Mar 22  03:41 /var/db/scripts/jet/jrp.py
total files: 1

user@bng1>
user@bng1> edit 
Entering configuration mode
The configuration has been changed but not committed

[edit]
user@bng1# show system 
scripts {
    language python3;
}
extensions {
    providers {
        jnpr {
            license-type juniper deployment-scope commercial;
        }
    }
    extension-service {
        application {
            file jrp.py {
                arguments "-s 1.1.1.1 -m 1.1.1.1:1812-10.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-10.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj";
                daemonize;
                respawn-on-normal-exit;
                username root;
            }
        }
    }
}

[edit]
user@bng1# show interfaces lo0 
unit 0 {
    family inet {
        address 1.1.1.1/32;
    }
}

[edit]
user@bng1# show access radius-server 
1.1.1.1 {
    port 1812;
    accounting-port 1813;
    secret "$9$JfUi.QF/0BEP5BEcyW8ZUj"; ## SECRET-DATA
    timeout 1;
    retry 3;
    max-outstanding-requests 500;
    source-address 1.1.1.1;
}

[edit]
user@bng1# show access profile freeradius 
authentication-order radius;
radius {
    authentication-server 1.1.1.1;
    accounting-server 1.1.1.1;
}
accounting {
    order radius;
}

[edit]

```


#### freeradius server accounting data
```sh
Wed May 19 21:06:43 2021
	User-Name = "juniper"
	Acct-Status-Type = Start
	Acct-Session-Id = "228"
	Event-Timestamp = "May 20 2021 05:09:08 UTC"
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	Framed-Route = "90.0.0.0/24 203.0.0.100 100"
	Framed-Route = "90.0.1.0/24 203.0.0.100 200"
	Framed-Route = "90.0.2.0/24 203.0.0.100 1"
	Framed-Route = "90.0.3.0/24 203.0.0.100 1"
	Framed-Route = "90.0.4.0/24 203.0.0.100 200"
	Attr-26.4874.177 = 0x506f72742073706565643a2031303030303030306b
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "08:00:27:72:0c:be"
	Framed-IP-Address = 203.0.0.100
	Framed-IP-Netmask = 255.255.255.255
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "xe-0/1/0.demux0.3221225676:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "08:00:27:72:0c:be"
	Attr-26.4874.210 = 0x00000004
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "b26af242a26f747afcb774e666a075ea"
	Timestamp = 1621458403

Wed May 19 21:07:03 2021
	User-Name = "08:00:27:72:0c:be"
	Acct-Status-Type = Start
	Acct-Session-Id = "229"
	Event-Timestamp = "May 20 2021 05:09:27 UTC"
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Route = "91.0.0.0/24 203.0.0.101 100"
	Framed-Route = "91.0.1.0/24 203.0.0.101 200"
	Framed-Route = "91.0.2.0/24 203.0.0.101 1"
	Framed-Route = "91.0.3.0/24 203.0.0.101 1"
	Attr-26.4874.177 = 0x506f72742073706565643a2031303030303030306b
	Acct-Authentic = RADIUS
	ERX-Dhcp-Options = 0x3501013204cb0000650c07636c69656e7431370d011c02030f06770c2c2f1a792a
	ERX-Dhcp-Mac-Addr = "08:00:27:72:0c:be"
	Framed-IP-Address = 203.0.0.101
	Framed-IP-Netmask = 255.255.255.0
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "xe-0/1/0.demux0.3221225676:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "08:00:27:72:0c:be"
	Attr-26.4874.189 = 0xcb000001
	Attr-26.4874.210 = 0x00000004
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "8f4dcfbe7424f63019fe97e8a2283247"
	Timestamp = 1621458423
	
Wed May 19 21:17:14 2021
	User-Name = "juniper"
	Acct-Status-Type = Stop
	Acct-Session-Id = "228"
	Event-Timestamp = "May 20 2021 05:19:39 UTC"
	Acct-Input-Octets = 1596
	Acct-Output-Octets = 338
	Acct-Session-Time = 631
	Acct-Input-Packets = 42
	Acct-Output-Packets = 42
	Acct-Terminate-Cause = User-Request
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	Framed-Route = "90.0.0.0/24 203.0.0.100 100"
	Framed-Route = "90.0.1.0/24 203.0.0.100 200"
	Framed-Route = "90.0.2.0/24 203.0.0.100 1"
	Framed-Route = "90.0.3.0/24 203.0.0.100 1"
	Framed-Route = "90.0.4.0/24 203.0.0.100 200"
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "08:00:27:72:0c:be"
	Framed-IP-Address = 203.0.0.100
	Framed-IP-Netmask = 255.255.255.255
	ERX-Input-Gigapkts = 0
	Acct-Input-Gigawords = 0
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "xe-0/1/0.demux0.3221225676:100-200"
	NAS-Port-Type = Ethernet
	ERX-Output-Gigapkts = 0
	Acct-Output-Gigawords = 0
	ERX-IPv6-Acct-Input-Octets = 0
	ERX-IPv6-Acct-Output-Octets = 0
	ERX-IPv6-Acct-Input-Packets = 0
	ERX-IPv6-Acct-Output-Packets = 0
	ERX-IPv6-Acct-Input-Gigawords = 0
	ERX-IPv6-Acct-Output-Gigawords = 0
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "08:00:27:72:0c:be"
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "b26af242a26f747afcb774e666a075ea"
	Timestamp = 1621459034

Wed May 19 21:17:34 2021
	User-Name = "08:00:27:72:0c:be"
	Acct-Status-Type = Stop
	Acct-Session-Id = "229"
	Event-Timestamp = "May 20 2021 05:19:58 UTC"
	Acct-Input-Octets = 328
	Acct-Output-Octets = 20
	Acct-Session-Time = 631
	Acct-Input-Packets = 1
	Acct-Output-Packets = 1
	Acct-Terminate-Cause = User-Request
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Route = "91.0.0.0/24 203.0.0.101 100"
	Framed-Route = "91.0.1.0/24 203.0.0.101 200"
	Framed-Route = "91.0.2.0/24 203.0.0.101 1"
	Framed-Route = "91.0.3.0/24 203.0.0.101 1"
	Acct-Authentic = RADIUS
	ERX-Dhcp-Options = 0x3501013204cb0000650c07636c69656e7431370d011c02030f06770c2c2f1a792a
	ERX-Dhcp-Mac-Addr = "08:00:27:72:0c:be"
	Framed-IP-Address = 203.0.0.101
	Framed-IP-Netmask = 255.255.255.0
	ERX-Input-Gigapkts = 0
	Acct-Input-Gigawords = 0
	NAS-Identifier = "alpha"
	NAS-Port = 200
	NAS-Port-Id = "xe-0/1/0.demux0.3221225676:100-200"
	NAS-Port-Type = Ethernet
	ERX-Output-Gigapkts = 0
	Acct-Output-Gigawords = 0
	ERX-IPv6-Acct-Input-Octets = 0
	ERX-IPv6-Acct-Output-Octets = 0
	ERX-IPv6-Acct-Input-Packets = 0
	ERX-IPv6-Acct-Output-Packets = 0
	ERX-IPv6-Acct-Input-Gigawords = 0
	ERX-IPv6-Acct-Output-Gigawords = 0
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "08:00:27:72:0c:be"
	Attr-26.4874.189 = 0xcb000001
	NAS-IP-Address = 1.1.1.1
	Tmp-String-9 = "ai:"
	Acct-Unique-Session-Id = "8f4dcfbe7424f63019fe97e8a2283247"
	Timestamp = 1621459054

	
```
#### Radius proxy log messages
```shell
root@alpha:~ # tail -f /var/log/jrp.log
2021-05-20 05:08:53.987 INFO 566: **********************
2021-05-20 05:08:53.988 INFO 567: radius proxy starts!!!
2021-05-20 05:08:53.988 INFO 568: **********************
2021-05-20 05:08:53.988 INFO 569: python version: 2.7.8
2021-05-20 05:08:53.988 DEBUG 570: arguments: ['/var/run/scripts/jet//jrp.py', '-m', '1.1.1.1:1812-10.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-10.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj']
2021-05-20 05:08:53.991 DEBUG 590: proxy mapping: 1.1.1.1:1812 <==> 10.0.0.1:1812 : $9$JfUi.QF/0BEP5BEcyW8ZUj
2021-05-20 05:08:53.992 DEBUG 590: proxy mapping: 1.1.1.1:1813 <==> 10.0.0.1:1813 : $9$JfUi.QF/0BEP5BEcyW8ZUj
2021-05-20 05:08:53.992 DEBUG 606: proxy packet source address: 1.1.1.1
2021-05-20 05:08:54.330 DEBUG 276: Reading junos configuration...Done.
2021-05-20 05:08:54.342 DEBUG 281: framed-route-table:
{
  "203.0.0.100": [
    "90.0.0.0/24 203.0.0.100 100",
    "90.0.1.0/24 203.0.0.100 200",
    "90.0.2.0/24 203.0.0.100 1",
    "90.0.3.0/24 203.0.0.100 1",
    "90.0.4.0/24 203.0.0.100 200"
  ],
  "203.0.0.101": [
    "91.0.0.0/24 203.0.0.101 100",
    "91.0.1.0/24 203.0.0.101 200",
    "91.0.2.0/24 203.0.0.101 1",
    "91.0.3.0/24 203.0.0.101 1"
  ]
}
2021-05-20 05:09:07.658 DEBUG 467: access-request      id: 222 pppoe-session: "0800.2772.0cbe" -> "08:00:27:72:0c:be"
2021-05-20 05:09:07.662 DEBUG 506: access-accept       id: 222 pppoe-session: "0800.2772.0cbe" -> framed-route append -> ["90.0.0.0/24 203.0.0.100 100", "90.0.1.0/24 203.0.0.100 200", "90.0.2.0/24 203.0.0.100 1", "90.0.3.0/24 203.0.0.100 1", "90.0.4.0/24 203.0.0.100 200"]
2021-05-20 05:09:07.957 DEBUG 467: accounting-request  id: 223 pppoe-session: "0800.2772.0cbe" -> "08:00:27:72:0c:be"
2021-05-20 05:09:27.799 DEBUG 467: access-request      id: 224 dhcp-session: "0800.2772.0cbe" -> "08:00:27:72:0c:be"
2021-05-20 05:09:27.801 DEBUG 506: access-accept       id: 224 dhcp-session: "0800.2772.0cbe" -> framed-route append -> ["91.0.0.0/24 203.0.0.101 100", "91.0.1.0/24 203.0.0.101 200", "91.0.2.0/24 203.0.0.101 1", "91.0.3.0/24 203.0.0.101 1"]
2021-05-20 05:09:27.899 DEBUG 467: accounting-request  id: 225 dhcp-session: "0800.2772.0cbe" -> "08:00:27:72:0c:be"

```

#### software version
##### JUNOS
```sh
Hostname: alpha
Model: mx204
Junos: 21.1R1.11
JUNOS OS Kernel 64-bit  [20210308.e5f5942_builder_stable_11]
JUNOS OS libs [20210308.e5f5942_builder_stable_11]
JUNOS OS runtime [20210308.e5f5942_builder_stable_11]
JUNOS OS time zone information [20210308.e5f5942_builder_stable_11]
JUNOS network stack and utilities [20210318.191732_builder_junos_211_r1]
JUNOS libs [20210318.191732_builder_junos_211_r1]
JUNOS OS libs compat32 [20210308.e5f5942_builder_stable_11]
JUNOS OS 32-bit compatibility [20210308.e5f5942_builder_stable_11]
JUNOS libs compat32 [20210318.191732_builder_junos_211_r1]
JUNOS runtime [20210318.191732_builder_junos_211_r1]
Junos vmguest package [20210318.191732_builder_junos_211_r1]
JUNOS sflow mx [20210318.191732_builder_junos_211_r1]
JUNOS py extensions [20210318.191732_builder_junos_211_r1]
JUNOS py base [20210318.191732_builder_junos_211_r1]
JUNOS OS vmguest [20210308.e5f5942_builder_stable_11]
JUNOS OS crypto [20210308.e5f5942_builder_stable_11]
JUNOS OS boot-ve files [20210308.e5f5942_builder_stable_11]
JUNOS na telemetry [21.1R1.11]
JUNOS Security Intelligence [20210318.191732_builder_junos_211_r1]
JUNOS mx libs compat32 [20210318.191732_builder_junos_211_r1]
JUNOS mx runtime [20210318.191732_builder_junos_211_r1]
JUNOS RPD Telemetry Application [21.1R1.11]
JUNOS Routing mpls-oam-basic [20210318.191732_builder_junos_211_r1]
JUNOS Routing mpls-oam-advanced [20210318.191732_builder_junos_211_r1]
JUNOS Routing lsys [20210318.191732_builder_junos_211_r1]
JUNOS Routing controller-internal [20210318.191732_builder_junos_211_r1]
JUNOS Routing controller-external [20210318.191732_builder_junos_211_r1]
JUNOS Routing 32-bit Compatible Version [20210318.191732_builder_junos_211_r1]
JUNOS Routing aggregated [20210318.191732_builder_junos_211_r1]
Redis [20210318.191732_builder_junos_211_r1]
JUNOS probe utility [20210318.191732_builder_junos_211_r1]
JUNOS common platform support [20210318.191732_builder_junos_211_r1]
JUNOS Openconfig [21.1R1.11]
JUNOS mtx network modules [20210318.191732_builder_junos_211_r1]
JUNOS modules [20210318.191732_builder_junos_211_r1]
JUNOS mx modules [20210318.191732_builder_junos_211_r1]
JUNOS mx libs [20210318.191732_builder_junos_211_r1]
JUNOS SQL Sync Daemon [20210318.191732_builder_junos_211_r1]
JUNOS mtx Data Plane Crypto Support [20210318.191732_builder_junos_211_r1]
JUNOS daemons [20210318.191732_builder_junos_211_r1]
JUNOS mx daemons [20210318.191732_builder_junos_211_r1]
JUNOS Broadband Egde User Plane Apps [21.1R1.11]
JUNOS appidd-mx application-identification daemon [20210318.191732_builder_junos_211_r1]
JUNOS TPM2 [20210318.191732_builder_junos_211_r1]
JUNOS Services URL Filter package [20210318.191732_builder_junos_211_r1]
JUNOS Services TLB Service PIC package [20210318.191732_builder_junos_211_r1]
JUNOS Services Telemetry [20210318.191732_builder_junos_211_r1]
JUNOS Services TCP-LOG [20210318.191732_builder_junos_211_r1]
JUNOS Services SSL [20210318.191732_builder_junos_211_r1]
JUNOS Services SOFTWIRE [20210318.191732_builder_junos_211_r1]
JUNOS Services Stateful Firewall [20210318.191732_builder_junos_211_r1]
JUNOS Services RTCOM [20210318.191732_builder_junos_211_r1]
JUNOS Services RPM [20210318.191732_builder_junos_211_r1]
JUNOS Services PCEF package [20210318.191732_builder_junos_211_r1]
JUNOS Services NAT [20210318.191732_builder_junos_211_r1]
JUNOS Services Mobile Subscriber Service Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services MobileNext Software package [20210318.191732_builder_junos_211_r1]
JUNOS Services Logging Report Framework package [20210318.191732_builder_junos_211_r1]
JUNOS Services LL-PDF Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services Jflow Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services Deep Packet Inspection package [20210318.191732_builder_junos_211_r1]
JUNOS Services IPSec [20210318.191732_builder_junos_211_r1]
JUNOS Services IDS [20210318.191732_builder_junos_211_r1]
JUNOS IDP Services [20210318.191732_builder_junos_211_r1]
JUNOS Services HTTP Content Management package [20210318.191732_builder_junos_211_r1]
JUNOS Services DNS Filter package (i386) [20210318.191732_builder_junos_211_r1] 
JUNOS Services Crypto [20210318.191732_builder_junos_211_r1]
JUNOS Services Captive Portal and Content Delivery Container package [20210318.191732_builder_junos_211_r1]
JUNOS Services COS [20210318.191732_builder_junos_211_r1]
JUNOS AppId Services [20210318.191732_builder_junos_211_r1]
JUNOS Services Application Level Gateways [20210318.191732_builder_junos_211_r1]
JUNOS Services AACL Container package [20210318.191732_builder_junos_211_r1]
JUNOS SDN Software Suite [20210318.191732_builder_junos_211_r1]
JUNOS Extension Toolkit [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (wrlinux9) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (MX/EX92XX Common) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (M/T Common) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (aft) [20210318.191732_builder_junos_211_r1]
JUNOS Packet Forwarding Engine Support (MX Common) [20210318.191732_builder_junos_211_r1]
JUNOS Juniper Malware Removal Tool (JMRT) [1.0.0+20210318.191732_builder_junos_211_r1]
JUNOS J-Insight [20210318.191732_builder_junos_211_r1]
JUNOS jfirmware [20210318.191732_builder_junos_211_r1]
JUNOS Online Documentation [20210318.191732_builder_junos_211_r1]
JUNOS jail runtime [20210308.e5f5942_builder_stable_11]
```

##### Python
```sh
python3.7
```